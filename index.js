const http = require("https");

const options = {
	"method": "GET",
	"hostname": "api-football-v1.p.rapidapi.com",
	"port": null,
	"path": "/v3/countries",
	"headers": {
		"x-rapidapi-key": "0446059184mshb6652a52843e1d5p174581jsn5e47135ecbff",
		"x-rapidapi-host": "api-football-v1.p.rapidapi.com",
		"useQueryString": true
	}
};

const req = http.request(options, function (res) {
	const chunks = [];

	res.on("data", function (chunk) {
		chunks.push(chunk);
	});

	res.on("end", function () {
		const body = Buffer.concat(chunks);
		console.log(body.toString());
	});
});

req.end();