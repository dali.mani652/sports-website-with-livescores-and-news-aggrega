//hide league list when screen width < 1200px;
// show league list when screen width > 1200px;
var leagues = document.getElementById("leagues") ;
var midpage = document.getElementById("midpage") ;
var button  = document.getElementById("league-btn") ;
var livescore = document.getElementById("livescore") ;
var leaguetable = document.getElementById("leaguetable") ;

width = screen.width ;
height = screen.height ;
console.log("your standard device resolution is: ",width,"px,",height,"px") ;

if (window.innerWidth > 900){
	button.disabled = true ;
} 

function controlList(){
	// show league's list
	if (leagues.style.display == "none"){
		leagues.style.display = "block" ; 
		leagues.style.width= "20%"
		midpage.style.width = "80%" ;
		midpage.style.clear = "none" ;
	}
	//hide league's list
	else {
		leagues.style.display = "none" ;
		midpage.style.width = "100%" ;
		livescore.style.width = "100%" ;
		midpage.style.clear = "left" ;
	}
}

 window.addEventListener("resize", function(event){
   	console.log(document.body.clientWidth) ;
    var width = document.body.clientWidth;

console.log("width after change : ",width) ;

if (width < 900){
	// mobile mode 
	button.disabled = false ;
	if (leagues.style.display == "none"){
		leagues.style.width = "0%" ;
		midpage.style.width = "100%" ;
		livescore.style.width = "100%" ;
		leaguetable.style.width = "100%" ;
	}
	else if (leagues.style.display == "block"){
	midpage.style.width = "80%" ;
	leagues.style.width = "20%" ;
	leaguetable.style.width = "100%" ;
	}
	console.log("mobile mode : ",width) ;
}
else{
 	//desktop mode
 	midpage.style.width = "60%" ;
 	leagues.style.width = "10%" ;
 	leaguetable.style.width = "30%" ;
 	console.log("desktop mode : ",width) ;
}

}) ;